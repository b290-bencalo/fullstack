Installation Guide

Syntax:
    npx create-react-app <folder-name>

Downgrading - npm install [package-name]@[version-number]

npx create-react-app react-booking-app - Creating react library

npm start - Start server

npm install bootstrap react-bootstrap

npm install react-router-dom - Installing react router dom

npm install sweetalert2 - sweet alert

Delete in package.json
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },

npm install mdbreact - for search button

Deploy react

https://vercel.com

Make sure react is a root folder

When creating projects in gitlab make sure to change the link to username instead of (B290-Bencalo change to pjbencalo)

ADD REACT_APP_API_URL in environment variables