import { useState, useEffect } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({ course }) {
  const { _id, name, description, price } = course;
  /* 
    props: {
      course:   {
        id: "wdc001",
        name: "PHP - Laravel",
        description:
          "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
        price: 45000,
        onOffer: true,
      },
    }
  */

  // Checks to see if the data was successfully passed
  /*   console.log("{course}: " + course); */

  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to individual components
  // Syntax
  // const [getter, setter] = useState(initialGetterValue);

  /*   const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30); */

  // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

  /*   console.log("useState(0): " + useState(0)); */

  /*   function enroll() {
    if (seats === 0) {
      alert("No more seats");
      return;
    }

    setCount(count + 1);
    console.log(`Enrollees: ${count}`);

    setSeats(seats - 1);
  } */

  /*  useEffect(() => {
    if (seats === 0) {
      alert("No more seats available");
    }
  }, [seats]); */
  return (
    <Row>
      <Col className="mb-3">
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <h5>Description:</h5>
            <p>{description}</p>
            <h5>Price:</h5>
            <p>PhP {price}</p>
            <Link className="btn btn-primary" to={`/courses/${_id}`}>
              Details
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
