import { Button, Col, Row } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { Link, NavLink } from "react-router-dom";

export default function Banner() {
  const location = useLocation();
  const { pathname } = location;
  const isHomePage = pathname === "/";
  return (
    <Row>
      {isHomePage ? (
        <Col className="p-5">
          <h1>Zuitt Coding Bootcamp</h1>
          <p>Opportunities for everyone, everywhere.</p>
          <Button variant="primary">Enroll Now</Button>
        </Col>
      ) : (
        <>
          <h3>Page Not Found</h3>
          <p>
            Go back to the {""}
            <NavLink to="/" className="text-decoration-none">
              homepage
            </NavLink>
            .
          </p>
        </>
      )}
    </Row>
  );
}

/* 
     - The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.
*/
