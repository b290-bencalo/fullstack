import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
  //  State hooks to store the values of the input fields
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  const [isValid, setIsValid] = useState(false);

  // Check if values are successfully bound
  /*   console.log(email);
  console.log(password1);
  console.log(password2); */
  // Function to simulate user registration
  function registerUser(e) {
    e.preventDefault();
    setFirstName("");
    setLastName("");
    setEmail("");
    setMobileNumber("");
    setPassword1("");
    setPassword2("");
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "error",
            title: "Duplicate email found",
            text: "Please provide a different email",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNumber,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data) {
                Swal.fire({
                  icon: "success",
                  title: "Registration successful",
                  text: "Welcome to Zuitt!",
                });
                navigate("/login");
              }
            })
            .catch((error) => {});
        }
      })
      .catch((error) => {});
  }
  // Validation to enable submit button when all fields are populated and both passwords match
  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNumber.match(/^\d+$/) &&
      mobileNumber.length >= 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2]);

  return user.id ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="email@mail.com"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone.
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="userNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter mobile number"
          value={mobileNumber}
          onChange={(e) => setMobileNumber(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter a password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="password2">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm your password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {/* Conditional Rendering for submit button based on isActive state */}
      {isActive ? (
        <Button
          variant="primary"
          type="submit"
          id="submit-button"
          className="mt-1"
        >
          Register
        </Button>
      ) : (
        <Button
          variant="primary"
          type="submit"
          id="submit-button"
          className="mt-1"
          disabled
        >
          Register
        </Button>
      )}
    </Form>
  );
}
