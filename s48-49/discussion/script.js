// console.log("asd");

// mock data
let posts = [];

// will be the id
let count = 1;

// CREATE (add post)

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();
  posts.push({
    id: count,
    title: document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value,
  });
  //   next available id (incremental value for id)
  count++;
  document.querySelector("#txt-title").value = null;
  document.querySelector("#txt-body").value = null;
  showPosts(posts);
  alert("Successfully added!");
});

const showPosts = (posts) => {
  let postEntries = "";
  // To iterate each posts from the API
  posts.forEach((post) => {
    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
      `;
  });
  // To check what is stored in the postEntries variables.
  console.log(postEntries);
  console.log(posts);
  // To assign the value of postEntries to the element with "div-post-entries" id.
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// edit button function
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;

  document.querySelector("#btn-submit-update").removeAttribute("disabled");
  let postId = document.querySelector("#txt-edit-id").value;
};

// UPDATE (update post)
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  let id = document.querySelector("#txt-edit-id").value;
  posts[id - 1].title = document.querySelector("#txt-edit-title").value;
  posts[id - 1].body = document.querySelector("#txt-edit-body").value;
  showPosts(posts);
  // Resetting disabled attribute for button
  document.querySelector("#txt-edit-title").value = null;
  document.querySelector("#txt-edit-body").value = null;
  // the value will be from the value of input fields or HTML elements
  document.querySelector("#btn-submit-update").setAttribute("disabled", true);
});
/* 
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();

  for (let i = 0; i < posts.length; i++) {
    // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
    // Therefore, it is necessary to convert the Number to a String first.
    if (
      posts[i].id.toString() === document.querySelector("#txt-edit-id").value
    ) {
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;
      showPosts(posts);
      alert("Successfully updated!");
      break;
    }
  }
}); */

const deletePost = (id) => {
  posts.splice(id - 1, 1);
  document.getElementById(`post-${id}`).remove();
  console.log(posts);
};
